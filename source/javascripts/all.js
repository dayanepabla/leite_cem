//= require jquery
//= require instafeed
//= require flexslider
//= require_tree .


$(document).ready(function() {
  // Scroll to section
  $('a[data-href]').on('click', function(event) {
    var self = $(this);
    var navbarOffset = 60;
    var target = $(self.data('href'));

    if (target.length) {
      event.preventDefault();

      $('html, body').animate({
        scrollTop: target.offset().top - navbarOffset
      }, 1000);
    }
  });

  $('.products-slider').flexslider({
    animation: "slide",
  });

  $('.navbar__toggle').click(function(event) {
    event.preventDefault();

    if ($('.navbar__nav').css('right') !== '0px') {
      $('.navbar__nav').css('right', '0px');
      $(this).css('right', '140px');
    } else {
      $('.navbar__nav').css('right', '-210px');
      $(this).css('right', '0px');
    }
  });

  var feed = new Instafeed({
    get: 'user',
    userId: '904390713',
    clientId: '682e9dbe7bcf4a53b1bf40ee07bbfa24',
    accessToken: '904390713.682e9db.942db0106cf14061a342fe3c4c288d56',
    limit: 8,
  });
  
  feed.run();
});
